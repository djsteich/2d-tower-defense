﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public float speed;
    private float maxDistance;

    public Transform target;
    void Update ()
    {
        if (target == null)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
        else
        {
            Vector3 diff = target.position - transform.position;
            transform.Translate(diff.normalized * Time.deltaTime * speed);
        }
        maxDistance += 1 * Time.deltaTime;
        if (maxDistance >= 3)
        {
            Destroy(gameObject);
        }
    }
}
