﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower360Controller : MonoBehaviour {
    private GameController gameController;
    private UpgradeButton360 upgradeButton360;
    public Transform shotSpawnN;
    public Transform shotSpawnNW;
    public Transform shotSpawnNE;
    public Transform shotSpawnS;
    public Transform shotSpawnSW;
    public Transform shotSpawnSE;
    public GameObject shot;
    public SphereCollider upgradedCollider;
    public Transform upgradedRange;

    public float fireRate; //in seconds
    public float fireWait;
    private float nextFire;
    private Collider targetEnemy;
    private bool isCoroutineRunning;
    private bool isEnemyThere;
    //private bool maxRangeUpgraded;
    //private bool maxFireRateUpgraded;
    private int towerRangeLevel;
    private int towerFireRateLevel;
    public int upgradeCostTracker;
    public int sellCostTracker;

    public void ClickIncreaseTrackers()
    {
        upgradeCostTracker += 400;
        sellCostTracker += 300;
    }

        

    public void ClickUpgrade()
    {
        //change variables here
        Debug.Log("Upgrade Function called");
        if (gameController.money >= upgradeCostTracker)
        {

            gameController.money -= upgradeCostTracker; //reduce money
            gameController.UpdateMoney();

            upgradedCollider.radius += .25f; //increase physical and logical radii
            upgradedRange.transform.localScale += new Vector3(.1f,.1f,.1f);
            towerRangeLevel++; //increase level var

            /*upgradeButton360.upgradeCost += 200; //increase upgrade/sell values and update Text obj
            upgradeButton360.sellCost += 100;
            upgradeButton360.UpdateUpgradeText();*/

            //maybe a separate ClickUpgradeRange and ClickUpgradeFireRate function but for now I guess this works
            //fireWait -= .25f; //increase fireRate
            gameController.T360Damage += 0; //increase damage
            towerFireRateLevel++; //increase level var
        }
    }

    public void ClickSell()
    {
        gameController.money += sellCostTracker;
        gameController.UpdateMoney();
        Destroy(gameObject); //destroy self
    }

    void Start () {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();

        /*GameObject upgradeButton360Object = GameObject.FindWithTag("UpgradeButton360");
        if (upgradeButton360Object != null)
            upgradeButton360 = upgradeButton360Object.GetComponent<UpgradeButton360>();*/

    }
	
	void Update () {
	    if (gameController.isWaveStarted == false)
        {
            StopAllCoroutines();
            isCoroutineRunning = false;
        }
    }

    /*void OnMouseOver()
    {
        Color rangeAlpha = Tower360Controller.GetComponent<SpriteRenderer>().color;
        rangeAlpha.a = .9f;
        Tower360Controller.GetComponent<SpriteRenderer>().color = rangeAlpha;
    }*/

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy") && Time.time >= nextFire && targetEnemy == null) //do I need Time.time here???
        {
            this.targetEnemy = other; //if there is no current targetEnemy, it is set here
            StopAllCoroutines();
            StartCoroutine(Fire());
            isCoroutineRunning = true;
            //Debug.Log("Fire() coroutine started");
        }

        if (other.CompareTag("Enemy") && targetEnemy != null)
        {
            this.targetEnemy = other; //once a new enemy enters the collider, that enemy becomes the targetEnemy
        }
        
            //Debug.Log("tower area entered");

    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("360TowerProjectile"))
        {
            if (other.gameObject.GetComponent<TowerOrigin>().originTower == this)
            {
                Destroy(other.gameObject);
            }
            
        }
            if (other.CompareTag("Enemy") && other == targetEnemy)
        {
            StopAllCoroutines();
            targetEnemy = null;
            isCoroutineRunning = false;
            //Debug.Log("tower area exited by " + other.gameObject);

        }
    }

    void OnTriggerStay(Collider other)
    {
        if (targetEnemy == null && other.CompareTag("Enemy"))
        {
            //Debug.Log("OnTriggerStay- target enemy set to " + other.gameObject);
            this.targetEnemy = other;

            if (isCoroutineRunning == false)
            {
                //Debug.Log("Fire()coroutine restarted");
                StartCoroutine(Fire());
                isCoroutineRunning = true;
            }
        }
    }

    IEnumerator Fire()
    {
        isCoroutineRunning = true;
        for (int i = 0; i<50; i++)
        {
            if (targetEnemy != null)
            {
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawnN.position, shotSpawnN.rotation).GetComponent<TowerOrigin>().originTower = this;
                Instantiate(shot, shotSpawnNW.position, shotSpawnNW.rotation).GetComponent<TowerOrigin>().originTower = this;
                Instantiate(shot, shotSpawnNE.position, shotSpawnNE.rotation).GetComponent<TowerOrigin>().originTower = this;
                Instantiate(shot, shotSpawnS.position, shotSpawnS.rotation).GetComponent<TowerOrigin>().originTower = this;
                Instantiate(shot, shotSpawnSW.position, shotSpawnSW.rotation).GetComponent<TowerOrigin>().originTower = this;
                Instantiate(shot, shotSpawnSE.position, shotSpawnSE.rotation).GetComponent<TowerOrigin>().originTower = this;

                yield return new WaitForSeconds(fireWait);
            }
            else
                yield return new WaitForEndOfFrame();
        }
        
        

    }
}
