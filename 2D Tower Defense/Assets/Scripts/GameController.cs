﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameController : MonoBehaviour {
    
    public GameObject [] enemies;
    public Transform enemySpawn;
    public int enemyCount;
    public int enemiesDefeated;
    public float enemyWait;
    public MovementPath newPath; //instance for enemies to follow
    #region Canvas Objects
    public Text waveText;
    public Text incomingText;
    public Text healthText;
    public Text moneyText;
    public Text towerText;
    public Text controlsText;
    public Text placeTowerText;
    public Text gameOverText;
    public Button startButton;
    public GameObject SFTowerOutline;
    public GameObject Tower360Outline;
    #endregion



    private int wave;
    public int health;
    public int money;
    public int towerBuilds = 2;
    private int enemyPool;
    public int sfCost;
    public int omniCost;
    public int difficultyMultiplier;
    public int SFDamage; //assign in editor
    public int T360Damage; //assign in editor


    public bool restart;
    public bool isWaveStarted;
    public bool singleFireSelected;
    public bool tower360selected;
    
    
    public void StartWaves() //called onClick for Start Button
    {
        //DOTween animations        
        startButton.transform.DOMove(new Vector3(10f, -3.5f, 0f), 1);
        controlsText.DOFade(0f, 0f);

        if (wave == 0)
        {
            incomingText.transform.DOMove(new Vector3(-5f, 3.6f, 0f), .3f);
            incomingText.DOFade(0f, 2f);
        }

        if (isWaveStarted ==false) //wave not started 
        {
            enemiesDefeated = 0;
            wave++;
            UpdateWave();
            enemyWait -= .01f;
            StartCoroutine(SpawnWaves());
            isWaveStarted = true;
            Debug.Log("Wave coroutine started");
            

            if (wave <= 5) //up to wave 5
            {
                enemyPool = enemies.Length - 4; //1st element of array
                //Debug.Log("enemy pool set to " + enemyPool);
            }
            if (wave <=9 && wave >5) //waves 6-9
            {
                difficultyMultiplier = 1;
                enemyPool = enemies.Length - 2; //1st and 2nd elements of array
                //Debug.Log("enemy pool set to " + enemyPool);
            }
            if (wave > 9 && wave <=12) //waves 10 to 12
            {
                difficultyMultiplier = 2;
                enemyPool = enemies.Length - 1; //2nd, and 3rd elements of array
               // Debug.Log("enemy pool set to " + enemyPool);
            }
            if (wave >12) //waves 13 to infinity 
            {
                enemyPool = enemies.Length;
            }
        }
        
    }

    public void SelectTower360()
    {
        tower360selected = true;
        Tower360Outline.SetActive(true);
        //Debug.Log("tower360selected set to true");
        singleFireSelected = false;
        SFTowerOutline.SetActive(false);
        controlsText.DOFade(0f, 0f);
        if (wave == 0)
        {
            placeTowerText.text = "Right Click to a place Tower\non your position!";
        }
    }

    public void SelectSingleFire()
    {
        singleFireSelected = true;
        SFTowerOutline.SetActive(true);
        //Debug.Log("singleFireSelected set to true");
        tower360selected = false;
        Tower360Outline.SetActive(false);
        controlsText.DOFade(0f, 0f);
        if (wave == 0)
        {
            placeTowerText.text = "Right Click to a place Tower\non your position!";
        }
    }

	void Start ()
    {
        difficultyMultiplier = 1;
        UpdateWave();
        UpdateHealth();
        //money = 300;
        UpdateMoney();
        UpdateTowerBuilds();
        controlsText.text = "Controls:\nWASD to move\nLeft Click to select\nSelect a Tower type to begin";
        gameOverText.text = "";
        placeTowerText.text = "";
        incomingText.text = "Incoming!";
        //StartCoroutine(SpawnWaves());
	}
	
	void Update ()
    {
        if (restart)
        {
            UpdateGameOver();
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("restart initiated");
                Application.LoadLevel(Application.loadedLevel);
            }
        }
        if (isWaveStarted == true)
        {
            if (enemiesDefeated >= enemyCount) //why is this being called multiple times? just want to be called once
            {
                isWaveStarted = false;
                startButton.transform.DOMove(new Vector3(5.6f, -3.5f, 0f), 1f); //move startButton back to original position
                Debug.Log("isWaveStarted set to false");
                enemiesDefeated = 0;
                enemyCount += (2 *difficultyMultiplier);
                    Debug.Log("enemyCount increased");
            }
        }

        


    }

    void UpdateWave()
    {
        waveText.text = "Wave: " + wave;
    }

    public void UpdateHealth()
    {
        healthText.text = "" + health;
    }

    public void UpdateMoney()
    {
        moneyText.text = "$" + money;
    }

    public void UpdateTowerBuilds()
    {
        towerText.text = "Tower Builds left: " + towerBuilds;
    }
    public void UpdateGameOver()
    {
        gameOverText.text = "Game Over!\nPress 'R' to Restart";
    }

    IEnumerator SpawnWaves()
    {
        for (int i = 0; i < enemyCount; i++)
        {
            

            
            GameObject enemy = enemies[Random.Range((difficultyMultiplier -1), enemyPool)];
            GameObject newEnemy = Instantiate(enemy, enemySpawn.position, enemySpawn.rotation);
             
           MovementPath newMP = new MovementPath();
            newMP.movementDirection = newPath.movementDirection;
            newMP.movingTo = newPath.movingTo;
            newMP.pathSequence = newPath.pathSequence;
            newMP.PathType = newPath.PathType;
            newEnemy.GetComponent<FollowPath>().SetPath(newMP);

            yield return new WaitForSeconds(enemyWait);
        }
        Debug.Log("for loop completed");

        yield break;
        //Debug.Log("wave var increased");
        
    }
}
