﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour {

    public float speed;
    private Vector3 moveInput;
    public Transform shotSpawn;
    public GameObject shot;
    public GameObject tower;
    public GameObject tower2;
    private Animator playerAnim;

    //private float nextFire; //time between shots
    public float fireRate;

    private GameController gameController;

	void Start ()
    {
        playerAnim = GetComponent<Animator>();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }
	
	void Update ()
    {

        FaceMouse();
        MovePlayer();

        /*if (gameController.isWaveStarted && Input.GetButton("Fire1") && Time.time > nextFire)
        {
            //Shoot();
        }*/

        if (gameController.tower360selected && Input.GetButtonDown("Fire2") && gameController.money >=gameController.omniCost)
        {   
            BuildTower();
            gameController.money -= gameController.omniCost;
            gameController.UpdateMoney();
            gameController.placeTowerText.DOFade(0f, .5f);
        }
        if (gameController.singleFireSelected && Input.GetButtonDown("Fire2") && gameController.money >= gameController.sfCost)
        {
            BuildSingleFireTower();
            gameController.money -= gameController.sfCost;
            gameController.UpdateMoney();
            gameController.placeTowerText.DOFade(0f, .5f);
        }


    }

    void MovePlayer() //move player with keys
    {
        float axisX = Input.GetAxis("Horizontal");
        playerAnim.SetFloat("SpeedX", axisX);

        float axisY = Input.GetAxis("Vertical");
        playerAnim.SetFloat("SpeedY",axisY);

        transform.Translate(new Vector3(axisX, axisY) * Time.deltaTime * speed, Space.World);

        if ((Input.GetAxis("Horizontal") < .1 && Input.GetAxis("Horizontal") > -.1) //check if player is moving
            && (Input.GetAxis("Vertical") < .1 && Input.GetAxis("Vertical") > -.1))
            playerAnim.SetBool("isIdle", true);

        else
            playerAnim.SetBool("isIdle",false);

        
        //binds player to Camera view
        var pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp(pos.x, 0.02f, 0.98f);
        pos.y = Mathf.Clamp(pos.y, 0.02f, 0.98f);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    void FaceMouse() //look towards mousePosition
    {

        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        Vector2 direction = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);

        transform.up = direction;
    }

    /*void Shoot()
    {
        nextFire = Time.time + fireRate;
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        
    }*/

    void BuildTower() //this might need to go in GameController
    {
        Instantiate(tower, transform.position, gameController.transform.rotation); //to make rotation 0
    }

    void BuildSingleFireTower()
    {
        Instantiate(tower2, transform.position, gameController.transform.rotation); //to make rotation 0
    }

}
