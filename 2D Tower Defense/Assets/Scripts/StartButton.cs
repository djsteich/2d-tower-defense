﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {

    public GameController gameController;

    void Start () {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }


    void Update ()
    {
        if (gameController.isWaveStarted)
        {
            GetComponent<Button>().interactable = false;
        }
        else GetComponent<Button>().interactable = true;
	}
}
