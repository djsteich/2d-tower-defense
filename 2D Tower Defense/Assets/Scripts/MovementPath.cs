﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPath : MonoBehaviour {

    public enum PathTypes //types of movement paths
    {
        linear, loop
    }

    public PathTypes PathType; //indicates the type of path(Linear or Looping)
    public int movementDirection = 1; //1 is clockwise/forward, -1 vice versa
    public int movingTo = 0; //used to identify point in path sequence we are moving to
    public Transform[] pathSequence; //array of all pts in path

	public void onDrawGizmos() //draws lines in editor to create path
    {
        if (pathSequence == null || pathSequence.Length <2)
        {
            return; //exits function
        }

        for (int i = 1; i<pathSequence.Length; i++)
        {
            //draw a line between the points
            Gizmos.DrawLine(pathSequence[i - 1].position, pathSequence[i].position);
        }

        if (PathType == PathTypes.loop)
        {
            Gizmos.DrawLine(pathSequence[0].position, pathSequence[pathSequence.Length - 1].position);
        }
    }

    public IEnumerator<Transform> GetNextPathPoint()
    {
        if (pathSequence == null || pathSequence.Length <2)
        {
            yield break;
        }

        while (true)
        {
            yield return pathSequence[movingTo];

            if (pathSequence.Length ==1)
            {
                continue;
            }

            if (PathType == PathTypes.linear)
            {
                if (movingTo <=0) //beginning of path
                {
                    movementDirection = 1; //set movement forward
                }
                //if you want to move backward
                /*else if (movingTo >= pathSequence.Length - 1)
                 * {
                 *      movementDirection = -1;
                 * }*/
            }

            movingTo = movingTo + movementDirection;

            if (movingTo == pathSequence.Length)
            {
                Debug.Log("movingTo =" + movingTo);
                //how to access enemy to destroy it
            }

            if (PathType == PathTypes.loop)
            {
                if (movingTo >= pathSequence.Length)
                {
                    movingTo = 0;
                }

                if (movingTo <0)
                {
                    movingTo = pathSequence.Length - 1;
                }
            }

        }
    }
}
