﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayRange : MonoBehaviour
{
    public GameObject range;
    public GameObject canvasText;

    /*void OnMouseEnter()
    {
        range.SetActive(true);
        canvasText.SetActive(true);
        //Debug.Log("mouse entered Range Collider");
    }

    void OnMouseExit()
    {
        range.SetActive(false);
        canvasText.SetActive(false);
    }*/

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            range.SetActive(true);
            canvasText.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            range.SetActive(false);
            canvasText.SetActive(false);
        }
    }
}
