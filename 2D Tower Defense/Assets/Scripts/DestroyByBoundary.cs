﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DestroyByBoundary : MonoBehaviour {

    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }

    void Update()
    {
        if (gameController.health <=0)
        {
            gameController.restart = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Enemy") && gameController.health != 0)
        {
            gameController.health -= 1;
            gameController.UpdateHealth();
            Camera.main.DOShakePosition(1f,.2f, 10,90f); //shake camera when enemies 
            Destroy(other.gameObject);

            gameController.enemiesDefeated++;
            Debug.Log("enemiesDefeated = " + gameController.enemiesDefeated + "/" + gameController.enemyCount);
        }

        if (other.CompareTag("Projectile") || other.CompareTag("360TowerProjectile"))
        {
            Destroy(other.gameObject);
        }
    }
}
