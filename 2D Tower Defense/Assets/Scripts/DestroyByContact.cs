﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyByContact : MonoBehaviour
{
    public GameObject deathSound;
    public float health;
    private float staticHealth;
    //public int moneyValue;
    private GameController gameController;
    public GameObject moneyToSpawn;
    public Slider healthBar;

    void Start()
    {
        staticHealth = health;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();

        
    }

    void TakeDamage(int damage)
    {
        if (health<=0)
        {
            return;
        }
        health -= damage;
        

        //Debug.Log(health);
        if (health <= 0)
        {
            /*GameObject moneyDrop = */
            Instantiate(moneyToSpawn, this.transform.position, this.transform.rotation);
            Instantiate(deathSound, this.transform.position, this. transform.rotation);
            Destroy(gameObject); //self
            gameController.enemiesDefeated++; //increase enemiesDefeated value
            Debug.Log("enemiesDefeated = " + gameController.enemiesDefeated + "/" + gameController.enemyCount);
            //gameController.money += moneyValue;
            //gameController.UpdateMoney();
        }
    }

    void OnTriggerEnter(Collider other) 

        // how to make sure projectiles that are deleted are spawning from the tower instance not from overlapping tower collisions?
    {
        /*if (other.CompareTag("Projectile"))
        {
            //Debug.Log("enemy area entered by " + other.gameObject);
            health -= 1;
            //Debug.Log(health);
            Destroy(other.gameObject); //projectile
        }*/

    
           

        
        if (other.CompareTag("360TowerProjectile"))
        {
            Destroy(other.gameObject); //projectile
            TakeDamage(gameController.T360Damage);
            healthBar.value -= (4 / staticHealth);
        }
        else if (other.CompareTag("SingleFireTowerProjectile"))
        {
            Destroy(other.gameObject); //projectile
            TakeDamage(gameController.SFDamage);
            healthBar.value -= (5/staticHealth); //figure out the exact math later
        }
        
    }
}