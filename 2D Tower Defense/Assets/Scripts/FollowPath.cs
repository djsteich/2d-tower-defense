﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FollowPath : MonoBehaviour {

    public enum MovementType
    {
        MoveTowards, LerpTowards
    }

    public MovementType Type = MovementType.MoveTowards;
    private MovementPath MyPath;
    public float speed = 1;
    public float MaxDistanceToGoal;

    private IEnumerator<Transform> pointInPath; //used to reference points returned from MyPath.GetNextPathPoint
     


    public void SetPath (MovementPath path)
    {
        MyPath = path;
        pointInPath = MyPath.GetNextPathPoint();
        pointInPath.MoveNext();
        transform.position = pointInPath.Current.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Type == MovementType.MoveTowards)
        {
            transform.position = Vector3.MoveTowards(transform.position, pointInPath.Current.position, Time.deltaTime * speed);

        }
        else if (Type == MovementType.LerpTowards)
        {
            transform.position = Vector3.Lerp(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
        }

        var distanceSquared = (transform.position - pointInPath.Current.position).sqrMagnitude; //faster than using a V3.Distance method
        if (distanceSquared< MaxDistanceToGoal * MaxDistanceToGoal)
        {
            pointInPath.MoveNext();
        }
	}
}
