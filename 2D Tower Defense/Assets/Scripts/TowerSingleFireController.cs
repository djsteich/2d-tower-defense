﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerSingleFireController : MonoBehaviour {

    private GameController gameController;
    public GameObject shot;
    public SphereCollider upgradedCollider;
    public GameObject upgradedRange;
    private GameObject bulletToDestroy;
    public float fireRate; //in seconds
    private float nextFire;
    private bool isCoroutineRunning;
    private int enemyCounter;
    public float fireWait;
    public Collider targetEnemy;
    //private int towerRangeLevel;
    //private int towerFireRateLevel;
    public int upgradeCostTracker;
    public int sellCostTracker; 

    public void ClickIncreaseTrackers()
    {
        upgradeCostTracker += 250;
        sellCostTracker += 150;
    }

    public void ClickUpgrade()
    {
        //change variables here
        Debug.Log("Upgrade Function called");
        if (gameController.money >= upgradeCostTracker)
        {

            gameController.money -= upgradeCostTracker;
            gameController.UpdateMoney();

            upgradedCollider.radius += .25f; //increase physical and logical radii
            upgradedRange.transform.localScale += new Vector3(.1f, .1f, .1f);
            

            //fireWait -= .125f;
            gameController.SFDamage += 0; //increase damage
        }
    }

    public void ClickSell()
    {
        gameController.money += sellCostTracker;
        gameController.UpdateMoney();
        Destroy(gameObject); //destroy self
    }

    void Start ()
    {
        //StartCoroutine(Fire());
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();
    }
	
	void Update ()
    {
        if (gameController.isWaveStarted == false)
        {
            StopAllCoroutines();
            isCoroutineRunning = false;
        }
        if (targetEnemy !=null)
        {
            FaceTarget();
        }
        if (bulletToDestroy != null && targetEnemy == null)
        {
            Destroy(bulletToDestroy);
        }

    }

    void FaceTarget() //look towards targetPosition
    {

        Vector3 targetPos = targetEnemy.transform.position;
        //mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        Vector2 direction = new Vector2(-(targetPos.x - transform.position.x), -(targetPos.y - transform.position.y));

        transform.GetChild(0).GetChild(0).up = direction; //rotates just the body of the model

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        enemyCounter++;
        if (other.CompareTag("Enemy") && Time.time >= nextFire && targetEnemy == null)
        {
            StopAllCoroutines();
            StartCoroutine(Fire());
            isCoroutineRunning = true;
            this.targetEnemy = other;
            //Debug.Log("tagetEnemy set to " + targetEnemy + enemyCounter);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("SingleFireTowerProjectile"))
        {
            if (other.gameObject.GetComponent<TowerOrigin>().singleFireTowerOrigin == this)
            {
                Destroy(other.gameObject);
            }

        }
        if (other.CompareTag("Enemy") && other == targetEnemy)
        {
            StopAllCoroutines();
            targetEnemy = null;
                //Debug.Log("Shooting stopped. tower area exited by " + other.gameObject + enemyCounter);
            isCoroutineRunning = false;

        }
    }
    void OnTriggerStay(Collider other)
    {
        if (targetEnemy ==null && other.CompareTag("Enemy"))
        {
            //Debug.Log("OnTriggerStay- target enemy set to " + other.gameObject + enemyCounter);
            this.targetEnemy = other;

            if (isCoroutineRunning ==false)
            {
                //Debug.Log("Fire()coroutine restarted");
                StartCoroutine(Fire());
                isCoroutineRunning = true;
            }
        }
    }

    IEnumerator Fire()
    {
        isCoroutineRunning = true;
        //Debug.Log("isCoroutineRunning =" + isCoroutineRunning);
        //starting the Coroutine at beginning of script & using an if to check for targetEnemy
        /*while(true)
        {
            Debug.Log("while loop running" + Time.deltaTime);
            if (targetEnemy != null)
            {
                nextFire = Time.time + fireRate; //idk that this does anything

                GameObject bullet = Instantiate(shot, this.transform.position, this.transform.rotation);
                bullet.GetComponent<TowerOrigin>().singleFireTowerOrigin = this;
                bullet.GetComponent<Mover>().target = targetEnemy.gameObject.transform; //makes projectile a homing projectile
                yield return new WaitForSeconds(1f);
            }
            else
                yield return new WaitForEndOfFrame();*/

        //this current setup still allows for burst fire bc if targetEnemy is destroyed within range, targetEnemy is null & Coroutine starts over
        for (int i = 0; i < 500; i++)
        {

            if (targetEnemy != null)
            {
                nextFire = Time.time + fireRate; //idk that this does anything

                GameObject bullet = Instantiate(shot, this.transform.position, this.transform.rotation);
                bullet.GetComponent<TowerOrigin>().singleFireTowerOrigin = this;
                bullet.GetComponent<Mover>().target = targetEnemy.gameObject.transform; //makes projectile a homing projectile
                bulletToDestroy = bullet;
                yield return new WaitForSeconds(1f);
            }
            else
                yield return new WaitForEndOfFrame();
        }
    }
    
}
