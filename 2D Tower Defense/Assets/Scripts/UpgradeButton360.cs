﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UpgradeButton360 : MonoBehaviour {

    private GameController gameController;
    public Text upgradeText;
    public int upgradeCost;
    public int sellCost;
    private int upgradeLevel;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
            gameController = gameControllerObject.GetComponent<GameController>();

       
        UpdateUpgradeText();
    }

    public void UpdateUpgradeText()
    {
        upgradeText.text = "Upgrade: $" + upgradeCost + "\nSell: $" + sellCost;
        if (upgradeLevel >=3)
        {
            upgradeText.text = "Max Level\nSell: $" + sellCost;
        }
    }

    public void IncreaseUpgradeCosts()
    {
        upgradeLevel++;
        Debug.Log(upgradeLevel);
        if (upgradeLevel <= 3)
        {
            upgradeCost += 400;
            sellCost += 300;
        }
    }
    void Update()
    {
        if (gameController.money >= upgradeCost && upgradeLevel <3)
        {
            GetComponent<Button>().interactable = true;

        }
        else
            GetComponent<Button>().interactable = false;
    }
}
